---
title: Handouts
---
<p>When you are creating printed information to hand out, these are a few<br />
        guidelines which may prove helpful.</p>
<ol>
<li>Consider first, who is going to see the information. The general<br />
          public? College students and professors? People interested in art? The<br />
          press? Aim the information at the type of people who you are trying to<br />
          recruit. You may want to create several handouts for different<br />
          purposes.</li>
<li>Art work keeps people interested in reading what you have printed. Use<br />
          period looking art work that will photocopy clearly (photographs usually<br />
          do not work here). Remember to use only art works for which you own the<br />
          copyright or have formal permission from the copyright holder. SCA art is<br />
          not always free for general use; check with the artist before you use<br />
          it.</li>
<li>Do not try to give too much information in a handout, rather keep it<br />
          simple and let the reader know where to get more information.</li>
<li>These things should be included in every handout:
<p>          - information about the SCA in general (what the SCA is),<br />
          - information about the local group (what the local group does),<br />
          - the address of the Office of the Registrar,<br />
          - the real name and phone number of a local contact person.<br />
          This insures that a person can find the SCA at a later date, even if the<br />
          person is living in another area.</p></li>
<li>Use 20th century language, not SCA jargon. (This applies to<br />
          demonstrations, too). Also avoid words that have a different meaning in<br />
          the SCA than in the real world, i.e. mundane, killing blow, and peer. In<br />
          particular, do not include "gaming" in your handout, because the public<br />
          will assume this means 20th century fantasy games.</li>
<li>Remember that the SCA is a "non-profit, educational organisation", if<br />
          you choose to include that information.</li>
</ol>
