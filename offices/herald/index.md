---
title: Kingdom Herald
excerpt: Heralds bring alive the pageantry of the medieval era
---

Heralds in the Middle Ages were specialists in chivalry, tournaments, coats-of-arms and the technicalities of war.  Heralds in the Society are experts in communicating tournament combat, court ceremony, and the specifics of historical names and coats-of-arms.

Heralds bring pageantry to the Society: we help make a modern experience turn into a near-medieval one. We do this in several ways.

* [Heraldry in the SCA]({{ site.baseurl }}{% link offices/herald/heraldryinthesca.md %})
* [Kingdom Awards and Orders]({{ site.baseurl }}{% link offices/herald/drachenwaldawardsorders.md %})
* [Before the Internet...there were heralds]({{ site.baseurl }}{% link offices/herald/heraldsbeforetheinternet.md %})
* [Drachenwald Order of Precedence, which includes a form for award recommendations](http://op.drachenwald.sca.org/op)
* [Registering names and heraldry in Drachenwald]({{ site.baseurl }}{% link offices/herald/registeringnamesheraldry.md %})
* [Drachenwald customs: some inter-kingdom anthropology]({{ site.baseurl }}{% link offices/herald/customsandinterkingdomanthropology.md %})
* [Field heralding crash course]({{ site.baseurl }}{% link offices/herald/fieldheraldrycrashcourse.md %})
* [The Zen of good court: building the special occasions of the Society]({{ site.baseurl }}{% link offices/herald/zenofgoodcourtguidance.md %})
* [Drachenwald Book of Ceremonies, version 3, Jan 2017, AS 51]({{ site.baseurl }}{% link offices/herald/files/ceremonies-v3.pdf %})
* [Drachenwald Book of Ceremonies Apocrypha, Jan 2017, AS 51 (examples of ceremonies)]({{ site.baseurl }}{% link offices/herald/files/drachenwald_apocrypha.pdf %})

# Learning and commenting on heraldry in Drachenwald: tools and documents 

* [SENA, Standards for Evaluation of Names and Armory: the rules all heralds use to review submissions](http://heraldry.sca.org/sena.html)
* [OSCAR, Online System for Commentary and Response, where heralds gather online](https://oscar.sca.org/index.php?action=181)
* [LoARs: Archive of Letters of Acceptance and Return](http://heraldry.sca.org/loar/)
* [SCA Armorial, every herald's favourite database](http://oanda.sca.org/)
* [East Kingdom Herald University](https://elmet.eastkingdom.org/ekhu/)

# Society-wide College of Arms

* [SCA College of Arms on the Web](http://heraldry.sca.org/welcome.html)
* [Administrative handbook: defines what we register, what we protect, our submission procedures and heraldic responsibilities in the SCA](http://heraldry.sca.org/admin.html)

{% include officer-contacts.html %}